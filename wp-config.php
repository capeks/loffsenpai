<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'lTb;X<L9nIe 2AA2pF}2D9]NH%JE0Lbqn.aJ!l}Ymzd/4pu!@AzOR*H:bA<]@2#7' );
define( 'SECURE_AUTH_KEY',  '5oYXGJs@-|B)S{i@Z>?ndWYprZ)dK@pdKNxTQ7H:wt8{ik7{H3*zdjM7.ZoZ[[)_' );
define( 'LOGGED_IN_KEY',    'wpqDDyE{Z5jDZS,B7cTTUg.pyk22Ik?LT9_T?,KS3=&HF??mj46=XBLo3Ybq{ZWS' );
define( 'NONCE_KEY',        'fGbFNMT4M>iki>>I%q6K5zVc8,?a%oL9+2r<`v&/@cIyN AbY=hsJ;J$?605s_q;' );
define( 'AUTH_SALT',        '70siY:.A)5RuN?gVYe12y74k|E9 Yt#7g[v}U18d$M[qtqRYy@Qzg>CA98-#j,aJ' );
define( 'SECURE_AUTH_SALT', '+H(&cXPmLTkl5kj.8ps6HMj3&AFcH-zHwhbk1Uwy<PQhDK^W0noIHxu[l0QJNxG*' );
define( 'LOGGED_IN_SALT',   '>1#Eb;-kHyBBYZYvd3G_lnt?BY_j@N&:*m-k;#G7+T!z&S:<z e#WRE>2;>GffG)' );
define( 'NONCE_SALT',       '?3zaO0P?Zd,n/z%l@[:LLr5$<y6x@_]YOp5+pvvz$UK*`Dg/ob9h]e>Tvy_nA26<' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wordpress';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
